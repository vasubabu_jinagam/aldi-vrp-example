import com.google.ortools.constraintsolver.*;
import com.google.protobuf.Duration;
import com.opencsv.CSVWriter;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class VrpWithHaversign {
    static {
        System.loadLibrary("jniortools");
    }

    private static Integer numberOfVehicles = 1;

    public static void main(String[] args) throws IOException {
        File file = new File("./experiment.csv");
        FileWriter outputfile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputfile);
        String[] header = {"TestGroup", "Number Of Vehicles", "vehicleNumber", "Route Distance", "Total Distance", "Max Distance", "Route for a vehicle"};
        writer.writeNext(header);
        DataModel d = new DataModel();
        for (int i = 1; i <= 10; i++) {
            numberOfVehicles = i;
            execute(numberOfVehicles, writer, d);
        }
        writer.close();
    }

    private static void execute(int numberOfVehicles, CSVWriter writer, DataModel data) throws IOException {
        int vehicleCapacity = (int) Math.ceil((data.addresses.length - 1) * 1.0 / numberOfVehicles);
        Arrays.fill(data.vehicleCapacities, vehicleCapacity);
        Arrays.fill(data.demands, 1);
        data.demands[0] = 0;
        List<List<Long>> distanceMatrix = createDistanceMatrix(data);
        int vehicleNumber = data.vehicleCapacities.length;
        int depot = 0;
        RoutingIndexManager manager = new RoutingIndexManager(distanceMatrix.size(), vehicleNumber, depot);
        RoutingModel routing = new RoutingModel(manager);
        final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            int toNode = manager.indexToNode(toIndex);
            return distanceMatrix.get(fromNode).get(toNode);
        });
        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
        routing.addDimension(transitCallbackIndex, 0, 800000000, true, "Distance");
        RoutingDimension distanceDimension = routing.getMutableDimension("Distance");
        distanceDimension.setGlobalSpanCostCoefficient(100);
        final int demandCallbackIndex = routing.registerUnaryTransitCallback((long fromIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            return data.demands[fromNode];
        });
        routing.addDimensionWithVehicleCapacity(demandCallbackIndex, 0,
                data.vehicleCapacities,
                true,
                "Capacity");
        RoutingSearchParameters searchParameters = main
                .defaultRoutingSearchParameters()
                .toBuilder()
                .setTimeLimit(Duration.newBuilder().setSeconds(40).build())
                .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                .build();
        Assignment solution = routing.solveWithParameters(searchParameters);
        if (solution == null) {
            System.out.println("Error finding route");
            return;
        }


        printSolution(vehicleNumber, routing, manager, solution, writer);
    }

    static class DataModel {

        Map<Integer, String> testGroupAddressesMap = new HashMap<>();
        final String[] addresses = {
                "48.137555	11.562379", // Depot
                "48.15076	11.55682",
                "48.12426	11.53891",
                "48.14844	11.62111",
                "48.13781	11.52213",
                "48.10637	11.60011",
                "48.13899	11.57925",
                "48.14990	11.56623",
                "48.11160	11.58702",
                "48.10566	11.53209",
                "48.12549	11.57752",
                "48.13584	11.54189",
                "48.13063	11.58953",
                "48.17440	11.50321",
                "48.13037	11.57116",
                "48.16676	11.57435",
                "48.15799	11.52718",
                "48.08476	11.47234",
                "48.14489	11.58123",
                "48.10665	11.63189",
                "48.15379	11.56742",
                "48.12571	11.60551",
                "48.15267	11.58436",
                "48.14371	11.57580",
                "48.19222	11.45113",
                "48.15284	11.58657",
                "48.12686	11.51234",
                "48.07711	11.54675",
                "48.08515	11.55049",
                "48.09292	11.53597",
                "48.13563	11.55598",
                "48.14925	11.55139",
                "48.10919	11.60069",
                "48.15112	11.61871",
                "48.13123	11.59591",
                "48.12435	11.54223",
                "48.15368	11.56430",
                "48.09252	11.55424",
                "48.18219	11.44446",
                "48.13849	11.59808",
                "48.16703	11.47143",
                "48.16945	11.57725",
                "48.15268	11.57416",
                "48.16080	11.56546",
                "48.15317	11.57316",
                "48.12977	11.69035",
                "48.15347	11.56413",
                "48.12925	11.62456",
                "48.14791	11.56631",
                "48.16049	11.59832",
                "48.14337	11.56279"

        };
        long[] demands = new long[addresses.length];

        long[] vehicleCapacities = new long[numberOfVehicles];
    }

    private static void printSolution(int vehicleNumber, RoutingModel routing, RoutingIndexManager manager, Assignment solution, CSVWriter writer) {

        DecimalFormat df2 = new DecimalFormat("#.##");
        System.out.println("===============Route with " + vehicleNumber + " vehicles ===================");
        double totalRouteDistance = 0;
        double maxDistance = Integer.MIN_VALUE;
        for (int i = 0; i < vehicleNumber; ++i) {
            long index = routing.start(i);
            System.out.println("Route for Vehicle " + i + ":");
            long routeDistance = 0;
            StringBuilder route = new StringBuilder();
            while (!routing.isEnd(index)) {
                route.append(manager.indexToNode(index)).append(" -> ");
                long previousIndex = index;
                index = solution.value(routing.nextVar(index));
                routeDistance += routing.getArcCostForVehicle(previousIndex, index, i);
            }
            System.out.println(route.toString() + manager.indexToNode(index));
            double distance = (double) routeDistance / 1000;
            if (maxDistance < distance) {
                maxDistance = distance;
            }
            System.out.println("Distance of the route: " + distance + "km");
            totalRouteDistance += distance;
            String[] data1 = {"" + vehicleNumber, "" + vehicleNumber + "." + (i + 1), "" + distance, df2.format(totalRouteDistance), "" + maxDistance, route.toString() + manager.indexToNode(index)};
            writer.writeNext(data1);
        }
        System.out.println("Total route distance is: " + df2.format(totalRouteDistance) + "km");
        System.out.println("Max Distance is: " + df2.format(maxDistance) + "km");
        System.out.println("");
    }

    public static List<List<Long>> createDistanceMatrix(DataModel data) throws IOException {
        String[] destinationAddresses = data.addresses;
        List<List<Long>> distanceMatrix = new ArrayList<>();

        String[] originAddresses = data.addresses;
        for (String originAddress : originAddresses) {
            List<Long> distances = new ArrayList<>();
            for (String destinationAddress : destinationAddresses) {
                int distance = calculateDistanceInMeters(Double.parseDouble(originAddress.split("\t")[0].trim()), Double.parseDouble(originAddress.split("\t")[1].trim()),
                        Double.parseDouble(destinationAddress.split("\t")[0].trim()), Double.parseDouble(destinationAddress.split("\t")[1].trim()));
                distances.add((long) distance);
            }
            distanceMatrix.add(distances);
        }
        return distanceMatrix;
    }


    public static int calculateDistanceInMeters(double userLat, double userLng,
                                                double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
        int value = (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c)) * 1000;
        value += value * 0.3;
        return value;
    }

}
