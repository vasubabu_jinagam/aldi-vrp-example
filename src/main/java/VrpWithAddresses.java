import com.google.gson.Gson;
import com.google.ortools.constraintsolver.*;
import example.Element;
import example.Example;
import example.Row;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VrpWithAddresses {
    static {
        System.loadLibrary("jniortools");
    }


    public static void main(String[] args) throws IOException {
        DataModel data = new DataModel();
        List<List<Long>> distanceMatrix = createDistanceMatrix(data);
        for (List<Long> rowList : distanceMatrix) {
            for (Long e : rowList) {
                System.out.print(e + " ");
            }
            System.out.println();
        }
        int vehicleNumber = data.vehicleCapacities.length;
        int depot = 0;
        RoutingIndexManager manager = new RoutingIndexManager(distanceMatrix.size(), vehicleNumber, depot);
        RoutingModel routing = new RoutingModel(manager);
        final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            int toNode = manager.indexToNode(toIndex);
            return distanceMatrix.get(fromNode).get(toNode);
        });
        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
        routing.addDimension(transitCallbackIndex, 0, 80000, true, "Distance");
        RoutingDimension distanceDimension = routing.getMutableDimension("Distance");
        distanceDimension.setGlobalSpanCostCoefficient(100);
        final int demandCallbackIndex = routing.registerUnaryTransitCallback((long fromIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            return data.demands[fromNode];
        });
        routing.addDimensionWithVehicleCapacity(demandCallbackIndex, 0,
                data.vehicleCapacities,
                true,
                "Capacity");
        RoutingSearchParameters searchParameters = main
                .defaultRoutingSearchParameters()
                .toBuilder()
                .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                .build();
        Assignment solution = routing.solveWithParameters(searchParameters);
        if (solution == null) {
            System.out.println("Error finding route");
            return;
        }
        printSolution(vehicleNumber, routing, manager, solution);
    }

    static class DataModel {
        final String[] addresses = {
                "Bayerstraße 81, 80335 München, Germany",
                "Theresienhöhe 16, 80339 München, Germany",
                "Aberlestraße 52, 81371 München, Germany",
                "Friedenheimer Str. 104, 80686 München, Germany",
                "Bergmannstraße 24, 80339 München, Germany",
                "Sonnenstraße 5, 80331 München, Germany",
                "Augustenstraße 107, 80798 München, Germany",
                "Lothringer Str. 10, 81667 München, Germany",
                "Vogelweidepl. 1, 81677 München, Germany",
                "Klenzestraße 17, 80469 München, Germany"
        };
        final long[] demands = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        final long[] vehicleCapacities = {5, 5, 5};
        final String API_KEY = "AIzaSyAM4VezTCyjWBL59rfccxpDluUQ6Myj30k";
    }

    private static void printSolution(int vehicleNumber, RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
        long maxRouteDistance = 0;
        for (int i = 0; i < vehicleNumber; ++i) {
            long index = routing.start(i);
            System.out.println("Route for Vehicle " + i + ":");
            long routeDistance = 0;
            StringBuilder route = new StringBuilder();
            while (!routing.isEnd(index)) {
                route.append(manager.indexToNode(index)).append(" -> ");
                long previousIndex = index;
                index = solution.value(routing.nextVar(index));
                routeDistance += routing.getArcCostForVehicle(previousIndex, index, i);
            }
            System.out.println(route.toString() + manager.indexToNode(index));
            System.out.println("Distance of the route: " + routeDistance + "m");
            maxRouteDistance = Math.max(routeDistance, maxRouteDistance);
        }
        System.out.println("Maximum of the route distances: " + maxRouteDistance + "m");
    }

    public static List<List<Long>> createDistanceMatrix(DataModel data) throws IOException {
        int maxElements = 100;
        int numAddresses = data.addresses.length;
        int maxRows = Math.floorDiv(maxElements, numAddresses);
        int q = numAddresses / maxRows;
        int r = numAddresses % maxRows;
        String[] destinationAddresses = data.addresses;
        List<List<Long>> distanceMatrix = new ArrayList<>();
        for (int i = 0; i < q; i++) {
            String[] originAddresses = Arrays.copyOfRange(data.addresses, i * maxRows, (i + 1) * maxRows);
            Example example = sendRequest(data.API_KEY, originAddresses, destinationAddresses);
            distanceMatrix.addAll(buildDistanceMatrix(example));
        }
        if (r > 0) {
            String[] originAddresses = Arrays.copyOfRange(data.addresses, q * maxRows, q * maxRows + r);
            Example example = sendRequest(data.API_KEY, originAddresses, destinationAddresses);
            distanceMatrix.addAll(buildDistanceMatrix(example));
        }
        return distanceMatrix;
    }

    private static Example sendRequest(String apiKey, String[] originAddresses, String[] destinationAddresses) throws IOException {
        String request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial";
        String originAddressString = String.join("|", originAddresses).replace(" ", "+");
        String destinationAddressString = String.join("|", destinationAddresses).replace(" ", "+");
        request = request + "&origins=" + originAddressString + "&destinations=" + destinationAddressString + "&key=" + apiKey;
        URL url = new URL(request);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        InputStream in = connection.getInputStream();
        Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
        return new Gson().fromJson(reader, Example.class);
    }

    private static List<List<Long>> buildDistanceMatrix(Example example) {
        List<List<Long>> distanceMatrix = new ArrayList<>();
        for (Row row : example.getRows()) {
            List<Long> rowList = new ArrayList<>();
            for (Element element : row.getElements()) {
                rowList.add(Long.valueOf(element.getDistance().getValue()));
            }
            distanceMatrix.add(rowList);
        }
        return distanceMatrix;
    }
}