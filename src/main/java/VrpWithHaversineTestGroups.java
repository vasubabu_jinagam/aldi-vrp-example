import com.google.ortools.constraintsolver.*;
import com.google.protobuf.Duration;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VrpWithHaversineTestGroups {
    static {
        System.loadLibrary("jniortools");
    }

    public static void main(String[] args) throws IOException, CsvException {
        File file = new File("./experiment.csv");
        FileWriter outputfile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputfile);
        String[] header = {"Test Group", "Number Of Vehicles", "vehicleNumber", "Route Distance", "Total Distance", "Max Distance", "Route for a vehicle"};
        writer.writeNext(header);
        Reader reader = Files.newBufferedReader(Paths.get("./testgroups.csv"));
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
        List<String[]> records = csvReader.readAll();
        int c = 0;
        String group = "";
        for (int t = 0; t < 220; t++) {
            List<String> addresses = new ArrayList<>();
            List<String[]> recordSet = records.subList(t * 50, Math.min(t * 50 + 50, records.size()));
            for (String[] record : recordSet) {
                addresses.add(record[1] + "," + record[2]);
                group = record[0];
            }
            System.out.println("running for " + group);
            for (int vc = 1; vc <= 10; vc++) {
                DataModel data = new DataModel(addresses, vc);
                execute(data, writer, group);
                System.out.println(group);
            }
            System.out.println("done for " + group);
        }
        writer.close();
    }

    private static void execute(DataModel data, CSVWriter writer, String group) throws IOException {
        int vehicleCapacity = (int) Math.ceil((data.addresses.size() - 1) * 1.0 / data.vehicleCapacities.length);
        Arrays.fill(data.vehicleCapacities, vehicleCapacity);
        Arrays.fill(data.demands, 1);
        data.demands[0] = 0;
        List<List<Long>> distanceMatrix = createDistanceMatrix(data);
        int vehicleNumber = data.vehicleCapacities.length;
        int depot = 0;
        RoutingIndexManager manager = new RoutingIndexManager(distanceMatrix.size(), vehicleNumber, depot);
        RoutingModel routing = new RoutingModel(manager);
        final int transitCallbackIndex = routing.registerTransitCallback((long fromIndex, long toIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            int toNode = manager.indexToNode(toIndex);
            return distanceMatrix.get(fromNode).get(toNode);
        });
        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
        routing.addDimension(transitCallbackIndex, 0, 800000000, true, "Distance");
        RoutingDimension distanceDimension = routing.getMutableDimension("Distance");
        distanceDimension.setGlobalSpanCostCoefficient(100);
        final int demandCallbackIndex = routing.registerUnaryTransitCallback((long fromIndex) -> {
            int fromNode = manager.indexToNode(fromIndex);
            return data.demands[fromNode];
        });
        routing.addDimensionWithVehicleCapacity(demandCallbackIndex, 0,
                data.vehicleCapacities,
                true,
                "Capacity");
        RoutingSearchParameters searchParameters = main
                .defaultRoutingSearchParameters()
                .toBuilder()
                .setTimeLimit(Duration.newBuilder().setSeconds(40).build())
                .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                .build();
        Assignment solution = routing.solveWithParameters(searchParameters);
        if (solution == null) {
            System.out.println("Error finding route");
            return;
        }
        printSolution(vehicleNumber, routing, manager, solution, writer, group);
    }

    static class DataModel {
        List<String> addresses;
        long[] demands;
        long[] vehicleCapacities;

        public DataModel(List<String> addresses, int numberOfVehicles) {
            this.addresses = addresses;
            int vehicleCapacity = (int) Math.ceil((addresses.size() - 1) * 1.0 / numberOfVehicles);
            demands = new long[addresses.size()];
            vehicleCapacities = new long[numberOfVehicles];
            Arrays.fill(vehicleCapacities, vehicleCapacity);
            Arrays.fill(demands, 1);
            demands[0] = 0;
        }
    }

    private static void printSolution(int vehicleNumber, RoutingModel routing, RoutingIndexManager manager, Assignment solution, CSVWriter writer, String group) {
        DecimalFormat df2 = new DecimalFormat("#.##");
        System.out.println("===============Route with " + vehicleNumber + " vehicles ===================");
        double totalRouteDistance = 0;
        double maxDistance = Integer.MIN_VALUE;
        for (int i = 0; i < vehicleNumber; ++i) {
            long index = routing.start(i);
            System.out.println("Route for Vehicle " + i + ":");
            long routeDistance = 0;
            StringBuilder route = new StringBuilder();
            while (!routing.isEnd(index)) {
                route.append(manager.indexToNode(index)).append(" -> ");
                long previousIndex = index;
                index = solution.value(routing.nextVar(index));
                routeDistance += routing.getArcCostForVehicle(previousIndex, index, i);
            }
            System.out.println(route.toString() + manager.indexToNode(index));
            double distance = (double) routeDistance / 1000;
            if (maxDistance < distance) {
                maxDistance = distance;
            }
            System.out.println("Distance of the route: " + distance + "km");
            totalRouteDistance += distance;
            String[] data1 = {"" + group, "" + vehicleNumber, "" + vehicleNumber + "." + (i + 1), "" + distance, df2.format(totalRouteDistance), "" + maxDistance, route.toString() + manager.indexToNode(index)};
            writer.writeNext(data1);
        }
        System.out.println("Total route distance is: " + df2.format(totalRouteDistance) + "km");
        System.out.println("Max Distance is: " + df2.format(maxDistance) + "km");
        System.out.println("");
    }

    public static List<List<Long>> createDistanceMatrix(DataModel data) throws IOException {
        List<String> originAddresses = data.addresses;
        List<String> destinationAddresses = data.addresses;
        List<List<Long>> distanceMatrix = new ArrayList<>();
        for (String originAddress : originAddresses) {
            List<Long> distances = new ArrayList<>();
            String[] latLngsOrigin = originAddress.split(",");
            for (String destinationAddress : destinationAddresses) {
                String[] latLngsDestination = destinationAddress.split(",");
                int distance = calculateDistanceInMeters(Double.parseDouble(latLngsOrigin[0].trim()), Double.parseDouble(latLngsOrigin[1].trim()),
                        Double.parseDouble(latLngsDestination[0].trim()), Double.parseDouble(latLngsDestination[1].trim()));
                distances.add((long) distance);
            }
            distanceMatrix.add(distances);
        }
        return distanceMatrix;
    }

    public static int calculateDistanceInMeters(double userLat, double userLng,
                                                double venueLat, double venueLng) {
        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
        int value = (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c)) * 1000;
        value += value * 0.3;
        return value;
    }
}
